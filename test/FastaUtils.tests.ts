import { expect } from "chai";

import { BioSeq } from '../src/BioSeq'
import { BioSeqSet } from "../src/BioSeqSet";
import { FastaUtils } from "../src/FastaUtils";

describe('FastaUtils', () => {
  describe('parse', () => {
    it('should parse a string with 1 bio sequence and return a BioSeqSet object', () => {
      const header = 'seq1'
      const sequence = 'AAAACCC'
      const seqString = `>${header}\n${sequence}`
      const bioSeqs = [new BioSeq(header, sequence)]
      const expected = new BioSeqSet(bioSeqs)
      const fasta = new FastaUtils()
      const fastaObject = fasta.parse(seqString)
      expect(fastaObject.getHash()).eql(expected.getHash())
    })
    it('should parse a string with 1 bio sequence and extra line EOF and return a BioSeqSet object', () => {
      const header = 'seq1'
      const sequence = 'AAAACCC'
      const seqString = `>${header}\n${sequence}\n`
      const bioSeqs = [new BioSeq(header, sequence)]
      const expected = new BioSeqSet(bioSeqs)
      const fasta = new FastaUtils()
      const fastaObject = fasta.parse(seqString)
      expect(fastaObject.getHash()).eql(expected.getHash())
    })
    it('should parse a string with 1 bio sequence in multiple lines and return a BioSeqSet object', () => {
      const header = 'seq1'
      const sequence = 'AAAACCC\nAAAAA'
      const seqString = `>${header}\n${sequence}`
      const bioSeqs = [new BioSeq(header, sequence.replace('\n',''))]
      const expected = new BioSeqSet(bioSeqs)
      const fasta = new FastaUtils()
      const fastaObject = fasta.parse(seqString)
      expect(fastaObject.getHash()).eql(expected.getHash())
    })
    it('should parse a string with more than 1 bio sequence and return a BioSeqSet objects', () => {
      const header1 = 'seq1'
      const sequence1 = 'AAAACCC'
      const header2 = 'seq2'
      const sequence2 = 'DDDEEEEFFF'
      const seqString = `>${header1}\n${sequence1}\n>${header2}\n${sequence2}`
      const bioSeqs = [new BioSeq(header1, sequence1), new BioSeq(header2, sequence2)]
      const expected = new BioSeqSet(bioSeqs)
      const fasta = new FastaUtils()
      const fastaObject = fasta.parse(seqString)
      expect(fastaObject.getHash()).eql(expected.getHash())
    })
    it('should parse a string with more than 1, multi-line, bio sequence and return a BioSeqSet objects', () => {
      const header1 = 'seq1'
      const sequence1 = 'AAAACCC\nAAAAA'
      const header2 = 'seq2'
      const sequence2 = 'DDDEEEEFFF\nHHHHHHHHHHHH'
      const seqString = `>${header1}\n${sequence1}\n>${header2}\n${sequence2}`
      const bioSeqs = [new BioSeq(header1, sequence1.replace('\n','')), new BioSeq(header2, sequence2.replace('\n',''))]
      const expected = new BioSeqSet(bioSeqs)
      const fasta = new FastaUtils()
      const fastaObject = fasta.parse(seqString)
      expect(fastaObject.getHash()).eql(expected.getHash())
    })
    it('should ignore blank spaces', () => {
      const header1 = 'seq1'
      const sequence1 = 'AAAA CCC\nAA AAA'
      const seq1right = 'AAAACCCAAAAA'
      const header2 = 'seq2'
      const sequence2 = 'DDD EEE EFFF\nHHH HHHH'
      const seq2right = 'DDDEEEEFFFHHHHHHH'
      const seqString = `>${header1}\n${sequence1}\n>${header2}\n${sequence2}`
      const bioSeqs = [new BioSeq(header1, seq1right), new BioSeq(header2, seq2right)]
      const expected = new BioSeqSet(bioSeqs)
      const fasta = new FastaUtils()
      const fastaObject = fasta.parse(seqString)
      expect(fastaObject.getHash()).eql(expected.getHash())
    })
    it('should throw if not a FASTA', () => {
      const header1 = 'seq1'
      const sequence1 = 'AAAA CCC\nAA AAA'
      const seq1right = 'AAAACCCAAAAA'
      const header2 = 'seq2'
      const sequence2 = 'DDD EEE EFFF\nHHH HHHH'
      const seq2right = 'DDDEEEEFFFHHHHHHH'
      const seqString = `${header1}\n${sequence1}\n${header2}\n${sequence2}`
      const fasta = new FastaUtils()
      expect(() => fasta.parse(seqString)).to.throw(`invalid fasta`)
    })
  })
  describe('write', () => {
    it('should write a FASTA string from an array of BioSeq object', () => {
      const header = 'seq1'
      const sequence = 'AAAACCC'
      const seqString = `>${header}\n${sequence}\n`
      const bioSeqs = [new BioSeq(header, sequence)]
      const input = new BioSeqSet(bioSeqs)
      const fasta = new FastaUtils()
      const fastaString = fasta.write(input)
      expect(fastaString).eql(seqString)
    })
    it('should write a FASTA string with empty line at EOF', () => {
      const header = 'seq1'
      const sequence = 'AAAACCC'
      const bioSeqs = [new BioSeq(header, sequence)]
      const input = new BioSeqSet(bioSeqs)
      const fasta = new FastaUtils()
      const fastaString = fasta.write(input)
      expect(fastaString[fastaString.length - 1]).eql('\n')
    })
    it('should write a FASTA string from an array of BioSeq object with multiple entries', () => {
      const header1 = 'seq1'
      const sequence1 = 'AAAACCC'
      const header2 = 'seq2'
      const sequence2 = 'DDDEEEEFFF'
      const seqString = `>${header1}\n${sequence1}\n>${header2}\n${sequence2}\n`
      const bioSeqs = [new BioSeq(header1, sequence1), new BioSeq(header2, sequence2)]
      const input = new BioSeqSet(bioSeqs)
      const fasta = new FastaUtils()
      const fastaString = fasta.write(input)
      expect(fastaString).eql(seqString)
    })
  })
})