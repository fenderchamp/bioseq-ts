import { expect } from 'chai'

import { BioSeq } from '../src/BioSeq'

describe('BioSeq', () => {
  describe('attributes', () => {
    it('.seq should return the sequence as Sequence object', () => {
      const sequence = 'AACC'
      const header = 'seq1'
      const bioseq = new BioSeq(header, sequence)
      expect(bioseq.seq).eql(sequence)
    })
    it('.header should return the header', () => {
      const sequence = 'AACC'
      const header = 'seq1'
      const bioseq = new BioSeq(header, sequence)
      expect(bioseq.header).eql(header)
    })
  })
})