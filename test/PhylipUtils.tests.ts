import { expect } from "chai";

import { BioSeq } from '../src/BioSeq'
import { BioSeqSet } from "../src/BioSeqSet";
import { PhylipUtils } from "../src/PhylipUtils";

describe('PhylipUtils', () => {
  describe('parse', () => {
    it('should parse')
  })
  describe('write', () => {
    it('should write a PHYLIP string from an array of BioSeq object', () => {
      const header = 'seq1'
      const sequence = 'AAAACCC'
      const seqString = `1 7\n${header}      ${sequence}\n`
      const bioSeqs = [new BioSeq(header, sequence)]
      const input = new BioSeqSet(bioSeqs)
      const phylip = new PhylipUtils()
      const phylipString = phylip.write(input)
      expect(phylipString).eql(seqString)
    })
    it('should write a PHYLIP string with empty line at EOF', () => {
      const header = 'seq1'
      const sequence = 'AAAACCC'
      const bioSeqs = [new BioSeq(header, sequence)]
      const input = new BioSeqSet(bioSeqs)
      const phylip = new PhylipUtils()
      const phylipString = phylip.write(input)
      expect(phylipString[phylipString.length - 1]).eql('\n')
    })
    it('should write a PHYLIP string from an array of BioSeq object with multiple entries', () => {
      const header1 = 'seq1'
      const sequence1 = 'AAAACCC'
      const header2 = 'seq2'
      const sequence2 = 'DDDEEEE'
      const seqString = `2 7\n${header1}      ${sequence1}\n${header2}      ${sequence2}\n`
      const bioSeqs = [new BioSeq(header1, sequence1), new BioSeq(header2, sequence2)]
      const input = new BioSeqSet(bioSeqs)
      const phylip = new PhylipUtils()
      const phylipString = phylip.write(input)
      expect(phylipString).eql(seqString)
    })
    it('should trucate at 9 characters longer headers. compatible with original PHYLIP and RAxML', () => {
      const header1 = 'seq1.abcdNotToShowUp'
      const sequence1 = 'AAAACCC'
      const header2 = 'seq2'
      const sequence2 = 'DDDEEEE'
      const seqString = `2 7\nseq1.abcd ${sequence1}\n${header2}      ${sequence2}\n`
      const bioSeqs = [new BioSeq(header1, sequence1), new BioSeq(header2, sequence2)]
      const input = new BioSeqSet(bioSeqs)
      const phylip = new PhylipUtils()
      const phylipString = phylip.write(input)
      expect(phylipString).eql(seqString)
    })
    it('should throw if sequences are not in the same length', () => {
      const header1 = 'seq1'
      const sequence1 = 'AAAACCC'
      const header2 = 'seq2'
      const sequence2 = 'DDDEEEEE'
      const bioSeqs = [new BioSeq(header1, sequence1), new BioSeq(header2, sequence2)]
      const input = new BioSeqSet(bioSeqs)
      const phylip = new PhylipUtils()
      expect(() => phylip.write(input)).to.throw('Not an alignment. Sequences must be all the same length (with gaps).')
    })
  })
})