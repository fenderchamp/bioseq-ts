import makeAseq from '@biowonks/aseq';

class Sequence {
  public readonly seq: string;

  constructor(sequence: string) {
    this.seq = sequence;
  }

  /**
   * Returns the sequence without gaps. It does not update the sequence.
   *
   * @returns
   * @memberof Sequence
   */
  public noGaps() {
    return this.seq.replace(/-/g, '');
  }

  /**
   * Returns the biowonks aseq id of the sequence
   *
   * @returns
   * @memberof Sequence
   */
  public aseq() {
    const gaplessSeq = this.noGaps();
    return makeAseq(gaplessSeq);
  }
}

export { Sequence };
