import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';
import { BioSeqSet } from './BioSeqSet';

abstract class ParseUtils {
  protected abstract log: Logger;
  protected abstract logLevel: LogLevelDescType;

  public abstract parse(chunk: string): BioSeqSet;
  public abstract write(input: BioSeqSet): string;
}

export { ParseUtils };
