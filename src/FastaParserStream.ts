import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';
import { Transform } from 'stream';
import { FastaUtils } from './FastaUtils';

/**
 * Transform stream takes fasta chunks and push BioSeq objects
 *
 * @class FastaParserStream
 * @extends {Transform}
 */
class FastaParserStream extends Transform {
  protected buffer: string;
  protected readonly logLevel: LogLevelDescType;
  protected readonly log: Logger;
  protected readonly fu: FastaUtils;

  constructor(logLevel: LogLevelDescType = 'INFO') {
    super({ objectMode: true });
    this.buffer = '';
    this.logLevel = logLevel;
    this.log = new Logger(this.logLevel);
    this.fu = new FastaUtils(this.logLevel);
  }

  public _transform(chunk: Buffer, enc: any, next: () => void) {
    const log = this.log.getLogger('FastaParserStream::transform');
    const raw = this.buffer + chunk.toString();
    const pieces = raw.split('>');
    const last = pieces.pop();
    if (last === '') {
      this.buffer = `${pieces.pop()}>`;
    } else {
      this.buffer = last || '';
    }
    // log.debug(this.buffer)
    this.fu
      .parse('>' + pieces.join('>'))
      .getBioSeqs()
      .forEach(entry => this.push(entry));
    next();
  }

  public _flush(next: () => void) {
    if (this.buffer !== '') {
      const set = this.fu
        .parse('>' + this.buffer)
        .getBioSeqs()
        .forEach(entry => this.push(entry));
    }
    next();
  }
}

export { FastaParserStream };
