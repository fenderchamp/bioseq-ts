<img src="https://gitlab.com/daviortega/bioseq-ts/raw/master/assets/BioSeqTS.logo.png" width=500px/>


![npm](https://img.shields.io/npm/v/bioseq-ts.svg)
![License: CC0-1.0](https://img.shields.io/badge/License-CC0%201.0-blue.svg)
[![pipeline status](https://gitlab.com/daviortega/bioseq-ts/badges/master/pipeline.svg)](https://gitlab.com/daviortega/bioseq-ts/commits/master)
[![coverage report](https://gitlab.com/daviortega/bioseq-ts/badges/master/coverage.svg)](https://daviortega.gitlab.io/bioseq-ts/coverage)

Biological Sequence in Javascript written in Typescript.

> Note: This package is under development and is in alpha. I build it for myself and others, but the functionalities appear as I need them. Please, use it at your own risk.

> Note2: That being said, please reach out to contribute, suggest changes and [submit issues](https://gitlab.com/daviortega/bioseq-ts/issues). Cheers.

# Usage

This package can be used by researchers at different levels of programming skills.

## Beginner

BioSeqTS provides high-level functionality if you are just starting to play with bioinformatics and just want a library that reads FASTA files for example.

We can easily accomplish that by:

```javascript
import fs from 'fs'
import { FastaUtils } from 'bioseq-ts'

const sequenceData = fs.readFileSync('myFile.fa')

const fu = new FastaUtils()
const bioSeqSet = fu.parse(sequenceData)

console.log(seqs)
/*
BioSeqSet {
  set: [
    BioSeq { seq: 'AAAACCC', header: 'seq1' },
    BioSeq { seq: 'DDDFFFF', header: 'seq2' },
    ...
  ],
  partitionTable: [ [ 1, 7 ] ] }
}
*/

```

But once we load these as `BioSeq` objects in a `BioSeqSet`, it packs a lot of other goodies.

For example, it is easy to write these sequences again in fasta format:

```javascript
const fu = new FastaUtils()
const fasta = fu.write(bioSeqSet)

console.log(fasta)
/*
>seq1
AAAACCC
>seq2
DDDFFFF
*/
```

## Intermediate

Let's say we want to concatenate several Fasta files. Assuming we already read the file content (string) into an array, let's write a simple function to concatenate the alignment.

```javascript
import { BioSeq, BioSeqSet, FastaUtils } from 'bioseq-ts';

const concatMultipleFasta = (ListOfRawSequencesInFasta: string[]): string => {
  const seqRefRaw = ListOfRawSequencesInFasta.shift();
  const fastaU = new FastaUtils();
  let concatenated = fastaU.parse(seqRefRaw);
  ListOfRawSequencesInFasta.forEach(rawSequencesInFasta => {
    const fu = new FastaUtils();
    const setToAdd = fu.parse(rawSequencesInFasta);
    concatenated = concatenated.concatSequences(setToAdd);
  });
  return fastaU.write(concatenated);
};

/*
This function returns a fasta formatted string with the concatenated alignment keeping the header of the first file.
*/

```

## Advanced

TODO

# Install

```shell
npm install bioseq-ts
```

# [Developer's Documentation](https://daviortega.gitlab.io/bioseq-ts/)

If you even more curious about the logic behind some of the choices we made here, please check it out the [dev README](https://gitlab.com/daviortega/bioseq-ts/blob/master/README.dev.md)

... to be continued.

Written with ❤ in Typescript.
